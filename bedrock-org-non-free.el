;; Pretty web interface for org-roam
(use-package org-roam-ui
  :after org-roam
  :config
  (setq org-roam-ui-sync-theme t
	org-roam-ui-follow t
	org-roam-ui-update-on-save t
	org-roam-ui-open-on-start t)
  :bind (("C-c n u" . org-roam-ui-mode)))

(provide 'bedrock-org-non-free)
